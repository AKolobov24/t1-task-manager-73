package ru.t1.akolobov.tm.web.service;

import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.model.Task;

@Service
public class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

}
