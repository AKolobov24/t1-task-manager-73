package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.IProjectRestEndpoint;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.util.UserUtil;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    public Project findById(
            @PathVariable("id")
            @NotNull final String id
    ) {
        return projectService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    public Project save(
            @RequestBody
            @NotNull final Project project
    ) {
        project.setUser(UserUtil.getUser());
        return projectService.save(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    public void deleteById(
            @PathVariable("id")
            @NotNull final String id
    ) {
        projectService.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

}
