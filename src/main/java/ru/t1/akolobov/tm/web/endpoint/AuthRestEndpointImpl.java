package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.akolobov.tm.web.api.endpoint.IAuthRestEndpoint;
import ru.t1.akolobov.tm.web.dto.Result;

@RestController
@RequestMapping("/api/auth")
public class AuthRestEndpointImpl implements IAuthRestEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Override
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(@RequestParam("username") @NotNull final String username,
                        @RequestParam("password") @NotNull final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            if (!authentication.isAuthenticated()) throw new AuthenticationServiceException(" ");
            return new Result(authentication.isAuthenticated());
        } catch (Exception e) {
            return new Result(e);
        }
    }

    @NotNull
    @Override
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
