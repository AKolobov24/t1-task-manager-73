package ru.t1.akolobov.tm.web.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.api.service.IService;
import ru.t1.akolobov.tm.web.model.AbstractModel;
import ru.t1.akolobov.tm.web.repository.CommonRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @Autowired
    private CommonRepository<M> commonRepository;

    @NotNull
    @Override
    public Collection<M> findAll() {
        return commonRepository.findAll();
    }

    @NotNull
    public Collection<M> findAllById(ArrayList<String> ids) {
        return commonRepository.findAllById(ids);
    }

    @Nullable
    @Override
    public M findById(@NotNull final String id) {
        return commonRepository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    @Transactional
    public M save(@NotNull final M model) {
        return commonRepository.save(model);
    }

    @Override
    @Transactional
    public void deleteById(@NotNull final String id) {
        if (!commonRepository.existsById(id)) throw new EntityNotFoundException();
        commonRepository.deleteById(id);
    }

}
