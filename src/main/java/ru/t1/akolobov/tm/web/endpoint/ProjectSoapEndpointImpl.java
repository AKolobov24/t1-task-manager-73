package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.dto.soap.project.*;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.util.UserUtil;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";
    public final static String NAMESPACE = "http://t1.ru/akolobov/tm/web/dto/soap/project";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "findAllProjectsRequest", namespace = NAMESPACE)
    public FindAllProjectsResponse findAllProjects(
            @NotNull
            @RequestPayload final FindAllProjectsRequest request
    ) {
        @NotNull final FindAllProjectsResponse response = new FindAllProjectsResponse();
        response.getReturn().addAll(projectService.findAllByUserId(UserUtil.getUserId()));
        return response;
    }


    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "findProjectByIdRequest", namespace = NAMESPACE)
    public FindProjectByIdResponse findProjectById(
            @NotNull
            @RequestPayload final FindProjectByIdRequest request
    ) {
        @NotNull final FindProjectByIdResponse response = new FindProjectByIdResponse();
        response.setReturn(
                projectService.findByUserIdAndId(
                        UserUtil.getUserId(),
                        request.getId()
                )
        );
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "saveProjectRequest", namespace = NAMESPACE)
    public SaveProjectResponse saveProject(
            @NotNull
            @RequestPayload final SaveProjectRequest request
    ) {
        @NotNull final SaveProjectResponse response = new SaveProjectResponse();
        @NotNull final Project project = request.getProject();
        project.setUser(UserUtil.getUser());
        response.setReturn(projectService.save(project));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "deleteProjectByIdRequest", namespace = NAMESPACE)
    public DeleteProjectByIdResponse deleteProject(
            @NotNull
            @RequestPayload final DeleteProjectByIdRequest request
    ) {
        projectService.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new DeleteProjectByIdResponse();
    }

}
