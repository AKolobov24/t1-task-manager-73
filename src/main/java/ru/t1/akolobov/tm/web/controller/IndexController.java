package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @NotNull
    @GetMapping("/")
    @PreAuthorize("isAuthenticated()")
    public String index() {
        return "index";
    }

}
