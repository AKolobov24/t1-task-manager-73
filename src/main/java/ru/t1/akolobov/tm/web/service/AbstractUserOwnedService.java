package ru.t1.akolobov.tm.web.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.api.service.IService;
import ru.t1.akolobov.tm.web.api.service.IUserOwnedService;
import ru.t1.akolobov.tm.web.model.AbstractModel;
import ru.t1.akolobov.tm.web.repository.CommonRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

public abstract class AbstractUserOwnedService<M extends AbstractModel> extends AbstractService<M>
        implements IService<M>, IUserOwnedService<M> {

    @Autowired
    private CommonRepository<M> commonRepository;

    @NotNull
    public Collection<M> findAllByUserId(@NotNull final String userId) {
        return commonRepository.findAllByUserId(userId);
    }

    @Nullable
    public M findByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        return commonRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id) {
        if (!commonRepository.existsByUserIdAndId(userId, id)) throw new EntityNotFoundException();
        commonRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@NotNull final String userId) {
        commonRepository.deleteAllByUserId(userId);
    }

}
