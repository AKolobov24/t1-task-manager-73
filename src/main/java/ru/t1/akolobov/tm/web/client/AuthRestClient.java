package ru.t1.akolobov.tm.web.client;

import feign.Feign;
import feign.Response;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.akolobov.tm.web.dto.Result;

public interface AuthRestClient {

    String BASE_URL = "http://localhost:8080";

    String PATH = BASE_URL + "/api/auth/";

    static AuthRestClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthRestClient.class, PATH);
    }

    @NotNull
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    Response login(@RequestParam("username") final String login,
                   @RequestParam("password") final String password
    );

    @NotNull
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    Result logout();

}
