package ru.t1.akolobov.tm.web.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "task")
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractModel {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public Task() {
        this.setName("New Task");
    }

    public Task(@NotNull final String name) {
        this.setName(name);
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
