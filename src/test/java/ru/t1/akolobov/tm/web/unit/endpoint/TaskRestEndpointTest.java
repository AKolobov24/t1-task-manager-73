package ru.t1.akolobov.tm.web.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.data.TestTask;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.service.UserService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskRestEndpointTest {

    private static final String TASK_URL = "/api/tasks";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private UserService userService;

    private User currentUser;

    private final User otherUser = new User("other", "other");

    private List<Task> taskList;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    public void addUsers() {
        currentUser = userService.findByLogin("user");
        Assert.assertNotNull(currentUser);
        if (userService.findById(otherUser.getId()) == null)
            userService.save(otherUser);
    }

    private void addData() {
        taskList = TestTask.createTaskList(currentUser);
        taskList.forEach(taskService::save);
        TestTask.createTaskList(otherUser).forEach(taskService::save);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        addUsers();
        addData();
        authenticate();
    }

    @After
    public void clear() {
        taskService.deleteAllByUserId(currentUser.getId());
        taskService.deleteAllByUserId(otherUser.getId());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final String methodUrl = TASK_URL + "/findAll";
        @NotNull final List<Task> tasks = Arrays.asList(
                objectMapper.readValue(
                        mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).contentType(MediaType.APPLICATION_JSON))
                                .andDo(print())
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(),
                        Task[].class
                )
        );
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void findById() throws Exception {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String methodUrl = TASK_URL + "/findById/" + task.getId();
        final Task responseTask = objectMapper.readValue(
                mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                Task.class
        );
        Assert.assertNotNull(responseTask);
        Assert.assertEquals(task, responseTask);
    }

    @Test
    public void save() throws Exception {
        @NotNull final String methodUrl = TASK_URL + "/save";
        @NotNull final Task newTask = TestTask.createTask(currentUser);
        @NotNull final String jsonRequest =
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(newTask);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(methodUrl)
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        final Task savedTask = taskService.findByUserIdAndId(currentUser.getId(), newTask.getId());
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(newTask, savedTask);
    }

    @Test
    public void deleteById() throws Exception {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String methodUrl = TASK_URL + "/deleteById/" + task.getId();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(methodUrl)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(taskService.findById(task.getId()));
    }

}
