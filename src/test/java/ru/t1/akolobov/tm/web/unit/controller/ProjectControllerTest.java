package ru.t1.akolobov.tm.web.unit.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.data.TestProject;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.service.UserService;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private UserService userService;

    private User currentUser;

    private List<Project> projectList;

    private void getUser() {
        currentUser = userService.findByLogin("user");
        Assert.assertNotNull(currentUser);
    }

    private void addData() {
        projectList = TestProject.createProjectList(currentUser);
        projectList.forEach(projectService::save);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        getUser();
        addData();
        authenticate();
    }

    @After
    public void clear() {
        projectService.deleteAllByUserId(currentUser.getId());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final String methodUrl = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(methodUrl))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void edit() throws Exception {
        @NotNull final String methodUrl = "/project/edit";
        mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).param("id", "0"))
                .andDo(print())
                .andExpect(status().is(404));

        @NotNull final Project project = projectList.get(0);
        Assert.assertEquals(
                project,
                mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).param("id", project.getId()))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getModelAndView().getModel().get("project"));
    }

    @Test
    public void create() throws Exception {
        @NotNull final String methodUrl = "/project/create";
        final String redirectUrl = mockMvc.perform(MockMvcRequestBuilders.get(methodUrl))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getResponse().getRedirectedUrl();

        Assert.assertNotNull(redirectUrl);
        Assert.assertFalse(redirectUrl.isEmpty());
        @NotNull final String[] splitUrl = redirectUrl.split("=");
        @NotNull final String newProjectId = splitUrl[1];
        Assert.assertEquals("/project/edit?id", splitUrl[0]);
        Assert.assertNotNull(projectService.findByUserIdAndId(currentUser.getId(), newProjectId));
    }

    @Test
    public void delete() throws Exception {
        @NotNull final String methodUrl = "/project/delete";
        @NotNull final Project project = projectList.get(0);
        Assert.assertNotNull(projectService.findById(project.getId()));
        final String redirectUrl = mockMvc.perform(
                        MockMvcRequestBuilders.post(methodUrl).param("id", project.getId())
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andReturn().getResponse().getRedirectedUrl();
        Assert.assertNull(projectService.findById(project.getId()));
        Assert.assertNotNull(redirectUrl);
        Assert.assertEquals("/projects", redirectUrl);
    }

}
