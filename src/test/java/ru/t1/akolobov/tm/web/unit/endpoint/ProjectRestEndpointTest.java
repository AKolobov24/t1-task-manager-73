package ru.t1.akolobov.tm.web.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.data.TestProject;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.service.UserService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectRestEndpointTest {

    private static final String PROJECT_URL = "/api/projects";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private UserService userService;

    private User currentUser;

    private final User otherUser = new User("other", "other");

    private List<Project> projectList;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    private void addUsers() {
        currentUser = userService.findByLogin("user");
        Assert.assertNotNull(currentUser);
        if (userService.findById(otherUser.getId()) == null)
            userService.save(otherUser);
    }

    private void addData() {
        projectList = TestProject.createProjectList(currentUser);
        projectList.forEach(projectService::save);
        TestProject.createProjectList(otherUser).forEach(projectService::save);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
        addUsers();
        addData();
        authenticate();
    }

    @After
    public void clear() {
        projectService.deleteAllByUserId(currentUser.getId());
        projectService.deleteAllByUserId(otherUser.getId());
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final String methodUrl = PROJECT_URL + "/findAll";
        @NotNull final List<Project> projects = Arrays.asList(
                objectMapper.readValue(
                        mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).contentType(MediaType.APPLICATION_JSON))
                                .andDo(print())
                                .andExpect(status().isOk())
                                .andReturn().getResponse().getContentAsString(),
                        Project[].class
                )
        );
        Assert.assertFalse(projects.isEmpty());
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findById() throws Exception {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String methodUrl = PROJECT_URL + "/findById/" + project.getId();
        final Project responseProject = objectMapper.readValue(
                mockMvc.perform(MockMvcRequestBuilders.get(methodUrl).contentType(MediaType.APPLICATION_JSON))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                Project.class
        );
        Assert.assertNotNull(responseProject);
        Assert.assertEquals(project, responseProject);
    }

    @Test
    public void save() throws Exception {
        @NotNull final String methodUrl = PROJECT_URL + "/save";
        @NotNull final Project newProject = TestProject.createProject(currentUser);
        @NotNull final String jsonRequest =
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(newProject);
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(methodUrl)
                                .content(jsonRequest)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        final Project savedProject = projectService.findByUserIdAndId(currentUser.getId(), newProject.getId());
        Assert.assertNotNull(savedProject);
        Assert.assertEquals(newProject, savedProject);
    }

    @Test
    public void deleteById() throws Exception {
        @NotNull final Project project = projectList.get(0);
        @NotNull final String methodUrl = PROJECT_URL + "/deleteById/" + project.getId();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(methodUrl)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(projectService.findById(project.getId()));
    }

}
