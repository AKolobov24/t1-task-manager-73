package ru.t1.akolobov.tm.web.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.data.TestTask;
import ru.t1.akolobov.tm.web.marker.UnitCategory;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.service.UserService;

import java.util.List;

@Transactional
@SpringBootTest
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskServiceTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private UserService userService;

    private final User currentUser = new User("current", "current");
    private final User otherUser = new User("other", "other");

    @Before
    public void addUsers() {
        userService.save(currentUser);
        userService.save(otherUser);
    }

    @After
    public void cleanData() {
        taskService.deleteAllByUserId(currentUser.getId());
        taskService.deleteAllByUserId(otherUser.getId());
    }

    @Test
    public void save() {
        @NotNull final Task task = TestTask.createTask(currentUser);
        taskService.save(task);
        Assert.assertTrue(taskService.findAllByUserId(currentUser.getId()).contains(task));
        Assert.assertEquals(1, taskService.findAllByUserId(currentUser.getId()).size());
    }

    @Test
    public void deleteAllByUserId() {
        @NotNull final List<Task> taskList = TestTask.createTaskList(currentUser);
        @NotNull final List<Task> user2TaskList = TestTask.createTaskList(otherUser);
        taskList.forEach(taskService::save);
        user2TaskList.forEach(taskService::save);
        long size = taskService.findAll().size();
        taskService.deleteAllByUserId(currentUser.getId());
        Assert.assertEquals(
                size - taskList.size(),
                taskService.findAll().size()
        );
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> user1TaskList = TestTask.createTaskList(currentUser);
        @NotNull final List<Task> user2TaskList = TestTask.createTaskList(otherUser);
        user1TaskList.forEach(taskService::save);
        user2TaskList.forEach(taskService::save);
        Assert.assertEquals(user1TaskList, taskService.findAllByUserId(currentUser.getId()));
        Assert.assertEquals(user2TaskList, taskService.findAllByUserId(otherUser.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final Task task = TestTask.createTask(currentUser);
        taskService.save(task);
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(task, taskService.findByUserIdAndId(currentUser.getId(), taskId));
        Assert.assertNull(taskService.findByUserIdAndId(otherUser.getId(), taskId));
    }

    @Test
    public void deleteByIdAndUserId() {
        TestTask.createTaskList(currentUser).forEach(taskService::save);
        @NotNull final Task task = TestTask.createTask(currentUser);
        taskService.save(task);
        taskService.deleteByUserIdAndId(currentUser.getId(), task.getId());
        Assert.assertNull(taskService.findByUserIdAndId(currentUser.getId(), task.getId()));
    }

}
