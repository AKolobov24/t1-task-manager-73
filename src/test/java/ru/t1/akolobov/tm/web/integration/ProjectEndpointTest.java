package ru.t1.akolobov.tm.web.integration;

import feign.Response;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.web.client.AuthRequestInterceptor;
import ru.t1.akolobov.tm.web.client.AuthRestClient;
import ru.t1.akolobov.tm.web.client.ProjectRestClient;
import ru.t1.akolobov.tm.web.marker.IntegrationCategory;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.User;

import java.net.HttpCookie;
import java.util.Arrays;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static final AuthRestClient authRestClient = AuthRestClient.client();

    @NotNull
    private static final User user = new User("admin", "admin");

    private static ProjectRestClient projectRestClient;

    @NotNull
    private final Project project1 = new Project("test-project-1");

    @NotNull
    private final Project project2 = new Project("test-project-2");

    @NotNull
    private final Project project3 = new Project("test-project-3");

    @BeforeClass
    public static void authenticate() {
        try (Response response = authRestClient.login(user.getLogin(), "admin")) {
            Assert.assertEquals(200, response.status());
            HttpCookie.parse(response.headers().get("set-cookie").toString()).stream()
                    .filter(httpCookie -> "[JSESSIONID".equals(httpCookie.getName()))
                    .findFirst()
                    .ifPresent((sessionCookie) ->
                            projectRestClient = ProjectRestClient.client(
                                    new AuthRequestInterceptor(sessionCookie.getValue())
                            )
                    );
        }
        Assert.assertNotNull(projectRestClient);
    }

    @AfterClass
    public static void logout() {
        authRestClient.logout();
    }

    @Before
    public void addData() {
        project1.setUser(user);
        project2.setUser(user);
        project3.setUser(user);
        projectRestClient.save(project1);
        projectRestClient.save(project2);
        projectRestClient.save(project3);
    }

    @After
    public void clearData() {
        if (projectRestClient.findById(project1.getId()) != null)
            projectRestClient.deleteById(project1.getId());
        if (projectRestClient.findById(project2.getId()) != null)
            projectRestClient.deleteById(project2.getId());
        if (projectRestClient.findById(project3.getId()) != null)
            projectRestClient.deleteById(project3.getId());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(
                projectRestClient
                        .findAll()
                        .containsAll(Arrays.asList(project1, project2, project3))
        );
    }

    @Test
    public void findById() {
        Assert.assertEquals(project1, projectRestClient.findById(project1.getId()));
        Assert.assertEquals(project2, projectRestClient.findById(project2.getId()));
        Assert.assertEquals(project3, projectRestClient.findById(project3.getId()));
    }

    @Test
    public void save() {
        @NotNull Project changedProject = new Project();
        changedProject.setId(project1.getId());
        changedProject.setName("test-project-changed");
        changedProject.setUser(user);
        projectRestClient.save(changedProject);
        Assert.assertEquals(changedProject, projectRestClient.findById(project1.getId()));

        @NotNull final Project newProject = new Project("new-project");
        newProject.setUser(user);
        projectRestClient.save(newProject);
        Assert.assertEquals(newProject, projectRestClient.findById(newProject.getId()));
        projectRestClient.deleteById(newProject.getId());
    }

    @Test
    public void deleteById() {
        projectRestClient.deleteById(project1.getId());
        Assert.assertNull(projectRestClient.findById(project1.getId()));
        Assert.assertFalse(projectRestClient.findAll().contains(project1));
    }

}
