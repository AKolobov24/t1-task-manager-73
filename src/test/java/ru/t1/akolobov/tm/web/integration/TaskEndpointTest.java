package ru.t1.akolobov.tm.web.integration;

import feign.Response;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.web.client.AuthRequestInterceptor;
import ru.t1.akolobov.tm.web.client.AuthRestClient;
import ru.t1.akolobov.tm.web.client.TaskRestClient;
import ru.t1.akolobov.tm.web.marker.IntegrationCategory;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.model.User;

import java.net.HttpCookie;
import java.util.Arrays;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final AuthRestClient authRestClient = AuthRestClient.client();

    @NotNull
    private static final User user = new User("admin", "admin");

    private static TaskRestClient taskRestClient;

    @NotNull
    private final Task task1 = new Task("test-task-1");

    @NotNull
    private final Task task2 = new Task("test-task-2");

    @NotNull
    private final Task task3 = new Task("test-task-3");

    @BeforeClass
    public static void authenticate() {
        try (Response response = authRestClient.login("admin", "admin")) {
            Assert.assertEquals(200, response.status());
            HttpCookie.parse(response.headers().get("set-cookie").toString()).stream()
                    .filter(httpCookie -> "[JSESSIONID".equals(httpCookie.getName()))
                    .findFirst()
                    .ifPresent((sessionCookie) ->
                            taskRestClient = TaskRestClient.client(
                                    new AuthRequestInterceptor(sessionCookie.getValue())
                            )
                    );
        }
        Assert.assertNotNull(taskRestClient);
    }

    @AfterClass
    public static void logout() {
        authRestClient.logout();
    }

    @Before
    public void addData() {
        task1.setUser(user);
        task2.setUser(user);
        task3.setUser(user);
        taskRestClient.save(task1);
        taskRestClient.save(task2);
        taskRestClient.save(task3);
    }

    @After
    public void clearData() {
        if (taskRestClient.findById(task1.getId()) != null)
            taskRestClient.deleteById(task1.getId());
        if (taskRestClient.findById(task2.getId()) != null)
            taskRestClient.deleteById(task2.getId());
        if (taskRestClient.findById(task3.getId()) != null)
            taskRestClient.deleteById(task3.getId());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(
                taskRestClient
                        .findAll()
                        .containsAll(Arrays.asList(task1, task2, task3))
        );
    }

    @Test
    public void findById() {
        Assert.assertEquals(task1, taskRestClient.findById(task1.getId()));
        Assert.assertEquals(task2, taskRestClient.findById(task2.getId()));
        Assert.assertEquals(task3, taskRestClient.findById(task3.getId()));
    }

    @Test
    public void save() {
        @NotNull Task changedTask = new Task();
        changedTask.setId(task1.getId());
        changedTask.setName("test-task-changed");
        changedTask.setUser(user);
        taskRestClient.save(changedTask);
        Assert.assertEquals(changedTask, taskRestClient.findById(task1.getId()));

        @NotNull final Task newTask = new Task("new-task");
        newTask.setUser(user);
        taskRestClient.save(newTask);
        Assert.assertEquals(newTask, taskRestClient.findById(newTask.getId()));
        taskRestClient.deleteById(newTask.getId());
    }

    @Test
    public void deleteById() {
        taskRestClient.deleteById(task1.getId());
        Assert.assertNull(taskRestClient.findById(task1.getId()));
        Assert.assertFalse(taskRestClient.findAll().contains(task1));
    }

}
